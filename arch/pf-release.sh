#!/usr/bin/env bash

source version
source targets

pf_tag="v${lin_x}.${lin_y}-pf${pf_ver}"
pf_rev=$(git -C linux rev-list -n 1 ${pf_tag})

for target in ${targets}; do
	if [[ "${target}" == "aur" ]]; then
		suffix=
	else
		suffix=-${target}
		target=linux-pf-${target}
		cp _service.template ${target}/_service
	fi

	config_sum=$(cksum --untagged -a blake2b ${target}/config | awk "{ print(\$1) }")

	cat PKGBUILD.template |
		sed "s|#TARGET#|${suffix}|" |
		sed "s|#PF_REV#|${pf_rev}|" |
		sed "s|#LIN_X#|${lin_x}|" |
		sed "s|#LIN_Y#|${lin_y}|" |
		sed "s|#PF_VER#|${pf_ver}|" |
		sed "s|#PKG_REL#|${pkg_rel}|" |
		sed "s|#CONFIG_SUM#|${config_sum}|" >${target}/PKGBUILD

	if [[ "${target}" == "aur" ]]; then
		makepkg --dir ${target} --printsrcinfo >${target}/.SRCINFO
	fi
done
